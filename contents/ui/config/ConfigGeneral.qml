import QtQuick 2.2
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import org.kde.plasma.core 2.0 as PlasmaCore

Item {
    property string cfg_currencies

    ListModel {
        id: currenciesModel
    }

    Component.onCompleted: {
        var currencies = JSON.parse(plasmoid.configuration.currencies)
        currencies.forEach(function (line) {
            currenciesModel.append({
                pairName: line.pairName,
                from: line.from,
                to: line.to,
            });
            
            console.debug("Line:", line.pairName, line.from, line.to);
        });
    }

    function currenciesModelChanged() {
        var newCurrenciesArray = []
        for (var i = 0; i < currenciesModel.count; i++) {
            newCurrenciesArray.push({
                pairName: currenciesModel.get(i).pairName,
                from: currenciesModel.get(i).from,
                to: currenciesModel.get(i).to,
            });
        }
        cfg_currencies = JSON.stringify(newCurrenciesArray)
    }

    Dialog {
        id: addCurrencyDialog
        title: i18n('Add Currency Pair')

        width: 400

        standardButtons: StandardButton.Ok | StandardButton.Cancel

        onAccepted: {
            if ((pairNameField.text !== "") && (fromField.text !== "") && (toField.text !== "")) { 
                console.log("Adding pair. Name:" + pairNameField.text);
                currenciesModel.append({pairName: pairNameField.text, from: fromField.text, to:toField.text});
                currenciesModelChanged()
                close()
            } else {
                console.log("Not adding, parameters not specified");
            }
        }

        GridLayout {
            anchors.fill: parent
            
            columns: 2
            
            Label {
                text: i18n('Name:')
            }
            TextField {
                id: pairNameField
                width: parent.width
                placeholderText: i18n('Displayed exchange pair name')
                Layout.fillWidth: true
            }
            
            Label {
                text: i18n('From:')
            }
            TextField {
                id: fromField
                width: parent.width
                placeholderText: i18n('Convert from currency')
                Layout.fillWidth: true
            }
            
            Label {
                text: i18n('To:')
            }
            TextField {
                id: toField
                placeholderText: i18n('Convert to currency')
                width: parent.width
                Layout.fillWidth: true
            }
        }
    }


    ColumnLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        
        Label {
            text: i18n('Currency Pairs')
            font.bold: true
            Layout.alignment: Qt.AlignLeft
        }
        
        TableView {
            id: currenciesTable
            width: parent.width
            
            TableViewColumn {
                id: nameCol
                role: 'pairName'
                title: i18n('Name')
                width: 250

                delegate: Label {
                    text: styleData.value
                    elide: Text.ElideRight
                    anchors.left: parent ? parent.left : undefined
                    anchors.leftMargin: 5
                    anchors.right: parent ? parent.right : undefined
                    anchors.rightMargin: 5
                }
            }

            TableViewColumn {
                id: fromCol
                role: 'from'
                title: i18n('From')
                width: 80

                delegate: Label {
                    text: styleData.value
                    elide: Text.ElideRight
                    anchors.left: parent ? parent.left : undefined
                    anchors.leftMargin: 5
                    anchors.right: parent ? parent.right : undefined
                    anchors.rightMargin: 5
                }
            }

            TableViewColumn {
                id: toCol
                role: 'to'
                title: i18n('To')
                width: 80

                delegate: Label {
                    text: styleData.value
                    elide: Text.ElideRight
                    anchors.left: parent ? parent.left : undefined
                    anchors.leftMargin: 5
                    anchors.right: parent ? parent.right : undefined
                    anchors.rightMargin: 5
                }
            }

            TableViewColumn {
                title: i18n('Action')
                width: parent.width * 0.2

                delegate: GridLayout {
                    height: parent.height
                    columns: 3
                    rowSpacing: 0

                    Button {
                        iconName: 'go-up'
                        Layout.fillHeight: true
                        onClicked: {
                            currenciesModel.move(styleData.row, styleData.row - 1, 1)
                            currenciesModelChanged()
                        }
                        enabled: styleData.row > 0
                    }

                    Button {
                        iconName: 'go-down'
                        Layout.fillHeight: true
                        onClicked: {
                            currenciesModel.move(styleData.row, styleData.row + 1, 1)
                            currenciesModelChanged()
                        }
                        enabled: styleData.row < currenciesModel.count - 1
                    }

                    Button {
                        iconName: 'list-remove'
                        Layout.fillHeight: true
                        onClicked: {
                            currenciesModel.remove(styleData.row)
                            currenciesModelChanged()
                        }
                    }
                }
            }
            
            model: currenciesModel
            Layout.preferredHeight: 150
            Layout.preferredWidth: parent.width
            Layout.columnSpan: 2
        }

        Row {
            Layout.columnSpan: 2
            
            Button {
                iconName: 'list-add'
                text: 'Add Currency Pair'
                onClicked: {
                    addCurrencyDialog.open();
                    pairNameField.text = '';
                    fromField.text = '';
                    toField.text = '';
                    pairNameField.focus = true
                }
            }
        }
    }
}
